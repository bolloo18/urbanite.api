import jwt from 'jsonwebtoken';

const authorizeApi  = (req, res,next) => {
    const token = req.cookies.token;

    if(token){
        jwt.verify(token,process.env.SECRET_KEY,(err,decodeToken)=>{
            if(err){
                console.log(err.message);
                res.status(403).json({status: "fail", message:"401 Not authorized"})
            }else{
                console.log(decodeToken);
                next();
            }
        });
    }else{
        res.status(401).json({status:"fail",message: '401 Not Authorized'});
    }
}


module.exports = {authorizeApi};