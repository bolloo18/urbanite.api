import jwt from 'jsonwebtoken';
import { auth } from '../config';
import Crypto from 'crypto';
import bcrypt from 'bcrypt';

//HASH PASSWORD
const hashPassword = (password) => {

    const salt = Crypto.randomBytes(Math.ceil(16/2)).toString('hex').slice(0,16);

    let hash = Crypto.createHmac('sha512',salt);
    hash.update(password);

    const value = hash.digest('hex');

    return{
        passwordSalt : salt,
        passwordHASH : value
    }; 
}

//VERIFY PASSWORD
const verifyHash = (salt, password) => {

    const hash = Crypto.createHmac('sha512',salt).update(password);

    return hash.digest('hex');
};

//HASH WITH BCRYPT
const pwdhash = (password)=>{
    
    try{
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(password,salt);
        
        return hash;
    }
    catch(err){
        console.log({"hash error=>":err.message});
    }
}
//TOKEN GENERATOR

const generateToken = (user)=>{
    
    const data = {
        sub: user.userID,
        email:user.email,
        role:user.userType
    };
    const signature = auth.SECRET_KEY;
    
    const expiration  = "3h";
    
    return jwt.sign(
        {
            data
        },
        signature,
        {
            algorithm: 'HS256',
            expiresIn:expiration
        }
    );
}

module.exports = {pwdhash, hashPassword, verifyHash, generateToken };