import dotenv from 'dotenv';

dotenv.config();

module.exports = {
    server: {
        port: process.env.PORT || process.env._PORT
    },
    auth : {
        SECRET_KEY: process.env.SECRET_KEY
    },
    env : {
        env : process.env.NODE_ENV || 'development'
    }
}