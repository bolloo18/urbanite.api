import express from 'express';
import CORS from 'cors';
import userRouter from '../user/user.routes';
import cookieParser from 'cookie-parser';

module.exports = (app)=>{
    app.use(express.urlencoded({ extended: true }));
    app.use(express.json());
    app.use(cookieParser());
    app.use(CORS());
    app.use(userRouter);

    return app;
}