import { server } from '../config';

import express from 'express';

async function startServer(){
     const app = express();
     
     require('./loaders/index')(app);
          
     await app.listen(server.port, (err) => {
          err? console.log(err.message) :  console.log(`App running on port : ${server.port}. Press CTRL + C to stop`);  
     });
}

startServer();