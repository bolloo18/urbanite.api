import Signup from "./Signup";
import Authenticate from "./Authenticate";
import {Profile} from "./Profile";

//signup
exports.signup = (req,res)=>{
   
  const {firstName,lastName,businessName,description,email,password} = req.body;
   
  switch(true){
    case (firstName===""):
        res.status(403).json({status:"fail", message  : "firstname is required"});
        break;
    case (lastName===""):
        res.status(403).json({status:"fail", message : "lastname is required"});
        break;
    case(!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)):
        res.status(403).json({status: "fail", message :"invalid email address"});
        break;
    case(businessName ===""):
        res.status(403).json({status: "fail", message:"business name is required"});
        break;    
    case(description===""):
        res.status(403).json({status: "fail", message:"business description is required"});
        break;
    case(password==="" || password.length<8):
        res.status(403).json({status: "fail", message:"password must be at least 8 characters"});
        break;
    default:
        Signup.signup(firstName,lastName,businessName,description,email,password).then(data=>{
            
        if(data.status==="error")return res.status(403).json(data);
        res.cookie('token',data.token,{httpOnly : true});
        return res.json(data);
    
        }).catch(err=>{
            return res.status(400).json({status:"error", message : err.message});
        });

  }

}

//Authenticate
exports.login = (req, res) => {
    const {email,password} = req.body;

    switch(true){
        case(!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)):
            return res.status(403).json({status: "fail", message: "invalid email address"});
            break;
        case(password===""):
            return res.status(403).json({status : "fail" ,message:"password is required"});
            break;
        default:
            Authenticate.login(email, password).then(data=>{
                if(data.status!=="success"){
                   return res.json(data);
                }{
                   const token = data.token;
                   res.cookie('token',token,{httpOnly : true});
                   res.json(data);
                }
            }).catch(err=>{
                return res.status(400).json({status:"error", message : err});
            });
    }
}

//update
exports.Update = (req,res)=>{
    const {userID,firstName,lastName,email,phone} = req.body;

    switch(true){
        case(!userID):
            return res.status(403).json({status : "fail",message: "userID is requires"});
        case(!firstName):
            return res.status(403).json({status : "fail", message : "firstName is require"});
        case(!lastName):
            return res.status(403).json({status:"fail",message:"lastName is required"});
        case(!email.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)):
            return res.status(403).json({status :"fail", message : "invalid email address"});
        case(!phone):
            return res.status(403).json({status : "fail", message : ""});        
        default:
            Profile.update({userID,firstName,lastName,email,phone})
            .then(data=>{                
                return  res.json(data);
            })            
            .catch((err) => {
                return res.status(403).json({status : "error",message : err.message });
            });
    }
}
