import express from 'express';
import Controller from './Controller';
import {authorizeApi} from '../../util/auth';

const userRouter = express.Router();

userRouter.post('/api/signup',Controller.signup);
userRouter.post('/api/authenticate',Controller.login);
userRouter.post('/api/user/profile/update',authorizeApi,Controller.Update);

module.exports = userRouter;