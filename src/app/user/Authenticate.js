import {PrismaClient} from '@prisma/client';
import jwtDecode from 'jwt-decode';
import {verifyHash,generateToken} from '../../util';
import bcrypt from 'bcrypt';

const prisma = new PrismaClient();
class Authenticate{
    static async login(email,password){

        try {
            const user = await prisma.useraccountview.findUnique({where : {email}});
            
            if(!user)return{status:"fail", message: "wrong email or password"};

            if(bcrypt.compareSync(password, user.password)===true){
                const userInfo = {
                    username : user.firstName+" "+user.lastName,
                    role : user.role,
                    accountStatus : user.accountStatus,
                    email : user.email,
                    joinDate : user.createdAt,
                    businessName : user.businessName,
                    busDescription : user.description,
                    userID : user.userID,
                    accountID : user.accountID
                }

                const token = generateToken(userInfo);
                const expiresAt = jwtDecode(token).exp;

                return{
                    status : "success",
                    message : "login successful",
                    userInfo,
                    token,
                    expiresAt
                }
            }
            else{

                return{
                     status : "fail",
                     message : "wrong email or password"
                }
            }
        } catch (error) {
            return {
                status : "error", message : error.message
            }
        }
    }


}

module.exports = Authenticate;