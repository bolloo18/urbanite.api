import { PrismaClient } from ".prisma/client";

const prisma = new PrismaClient();

class Profile {
    static async update({userID,firstName,lastName,email,phone}){

        
        try{
            const user = await prisma.user.update({
            where :{ userID },
            data:{
                email,
                firstName,
                lastName,
                phone,
                email,                
                logins : { set : {email} }
                }
            });

            if(!user){
                return{status : "fail", message : "update failed"}
            }else{
                return{
                    status : "success", message : "user update successful", data : user
                }
            }

        }catch(err){
            return{
                status : "error", message : err.message
            }
        }

    }

}
module.exports = {Profile};