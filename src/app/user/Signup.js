import { PrismaClient } from "@prisma/client";
import jwtDecode from "jwt-decode";
import {v4 as uuidv4} from 'uuid';
import { pwdhash,generateToken } from "../../util";

const prisma = new PrismaClient();
class Signup{

    static async signup(firstName,lastName,businessName,description,email,password){

        try {
        
            const user = await prisma.user.create({
                data: {
                    userID : uuidv4(),
                    firstName,
                    lastName,
                    role : "owner",
                    account : {
                        create : {
                            accountID : uuidv4(),
                            accountStatus : "active",
                             business : {
                                 create : {
                                     businessID : uuidv4(),
                                     businessName,
                                     description,
                                     email                               
                                 }
                             },
                             logins :{
                                 create : {
                                     loginID : uuidv4(),
                                     email,
                                     password : pwdhash(password)
                                 }
                             }
                        }
                    }
                }
            });
    
            const userInfo = {
                firstName : user.firstName,
                lastName : user.lastName,
                role : user.role,
                userID : user.userID,   
                createdAt : user.createdAt
            }
    
            const token = generateToken(userInfo);
            const expiresAt = jwtDecode(token).exp;

            return{
                status : "success",
                message : "Account created successfully",
                userInfo,
                expiresAt,
                token
            }
        } catch (error) {
            switch(true){
                case (error.code==="P2002" && error.meta.target==="email"):
                    return{status : "error",message : "Email already exists"};
                    break;                
                case (error.code==="P2002" && error.meta.target==="businessName"):
                    return{status : "error",message : "Business name is taken"};
                    break;
                default:
                     return{status : "error",message : error};
            }
        }
    }
}
export default Signup;